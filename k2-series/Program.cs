﻿using System;
using System.Windows;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Kinect;



using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using OpenCvSharp.Extensions;

using NAudio;
using NAudio.Wave;
using NAudio.Dsp;

namespace k2series
{
    class myK2
    {
        private Object thisLock = new Object();
        
        
        
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        public KinectSensor kinectSensor = null;
        
        /// <summary>
        /// Reader for body frames
        /// </summary>
        MultiSourceFrameReader multiReader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        public Body[] bodies = null;
        public int BodyCount = 6;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        public ushort depthWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        public ushort depthHeight;

        /// <summary>
        /// Intermediate storage for receiving depth data from the sensor
        /// </summary>
        public ushort[] depthData = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        public CoordinateMapper coordinateMapper = null;


        /// <summary>
        /// Width of display (infrared space)
        /// </summary>
        public ushort infraRedWidth;

        /// <summary>
        /// Height of display (infrared space)
        /// </summary>
        public ushort infraRedHeight;

        /// <summary>
        /// Intermediate storage for receiving infrared data from the sensor
        /// </summary>
        public ushort[] infraRedData = null;
        
        /// <summary>
        /// Width of display (body index space)
        /// </summary>
        public ushort bodyWidth;

        /// <summary>
        /// Height of display (bodyindex space)
        /// </summary>
        public ushort bodyHeight;

        /// <summary>
        /// Intermediate storage for receiving frame data from the sensor
        /// </summary>
        public byte[] bodyData = null;

 
        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        public int bytesPerPixel = 3;
        

        /// <summary>
        /// Pixel format currently in use (as of 2014-05-02 this is still yuy2
        /// </summary>
        public ColorImageFormat colorFormat= ColorImageFormat.Yuy2;
        /// <summary>
        /// Width of display (color space)
        /// </summary>
        public ushort colorWidth;
        public ushort smallColorWidth;
        /// <summary>
        /// Height of display (color space)
        /// </summary>
        public ushort colorHeight;
        public ushort smallColorHeight;

        /// <summary>
        /// Intermediate storage for frame data converted to color
        /// </summary>
        public byte[] colorData = null;
        public byte[] colorSmallData = null;

        public int fps = 12;
        public bool saveColor = false;
        // constructing myK2 we initialize the default sensor (single kinect scenario here)
        // initialize some data about depth/infrared/body source (at this time they are the same) 
        // and initialize the sensor to multisource capture

        private Mat depthCV = null;
        private Mat irCV = null;
        public Mat colorCV = null;

        public Timer ticker = new Timer();
        private BinaryWriter recorder = null;

        private TextWriter skelWriter = null;

        public VideoWriter colorWriter = null;
        
        //buffer to store acquired sound
        private byte[] audioBuffer = null;

        /// <summary>
        /// Number of bytes in each Kinect audio stream sample (32-bit IEEE float).
        /// </summary>
        private const int BytesPerSample = sizeof(float);

        /// <summary>
        /// Reader for audio frames
        /// </summary>
        private AudioBeamFrameReader audioReader = null;
        public float beamAngle = 0.0f;
        public float beamAngleConfidence=0.0f;
        public float horizontalFieldofView = 0.0f;
        public float[] audioSignal = null;

        public void init(int fps=12)
        {
            this.kinectSensor = KinectSensor.GetDefault();
            this.fps = fps;
           
            if (this.kinectSensor != null)
            {
                this.ticker = new Timer(1000 / fps);
                
                // get the coordinate mapper
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;
                
                // open the sensor
                this.kinectSensor.Open();
                // deal with audio :
                // Get its audio source
                AudioSource audioSource = this.kinectSensor.AudioSource;

                // Allocate 1024 bytes to hold a single audio sub frame. Duration sub frame 
                // is 16 msec, the sample rate is 16khz, which means 256 samples per sub frame. 
                // With 4 bytes per sample, that gives us 1024 bytes.
                this.audioBuffer = new byte[audioSource.SubFrameLengthInBytes];
                this.audioSignal = new float[audioSource.SubFrameLengthInBytes / BytesPerSample];
                // Open the reader for the audio frames
                this.audioReader = audioSource.OpenReader();
                
                // Uncomment these two lines to overwrite the automatic mode of the audio beam.
                // It will change the beam mode to manual and set the desired beam angle.
                // In this example, point it straight forward.
                // Note that setting beam mode and beam angle will only work if the
                // application window is in the foreground.
                // Furthermore, setting these values is an asynchronous operation --
                // it may take a short period of time for the beam to adjust.
                /*
                audioSource.AudioBeams[0].AudioBeamMode = AudioBeamMode.Manual;
                audioSource.AudioBeams[0].BeamAngle = 0;
                */



                if (this.audioReader != null)
                {
                    // Subscribe to new audio frame arrived events
                    this.audioReader.FrameArrived += this.Reader_FrameArrived;
                }

                // get the depth (display) extents
                FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                this.depthWidth = (ushort)frameDescription.Width;
                this.depthHeight = (ushort)frameDescription.Height;
                frameDescription = this.kinectSensor.InfraredFrameSource.FrameDescription;
                this.infraRedWidth = (ushort)frameDescription.Width;
                this.infraRedHeight = (ushort)frameDescription.Height;
                this.horizontalFieldofView = frameDescription.HorizontalFieldOfView;

                frameDescription = this.kinectSensor.BodyIndexFrameSource.FrameDescription;
                this.bodyWidth = (ushort)frameDescription.Width;
                this.bodyHeight = (ushort)frameDescription.Height;
               
                frameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
                this.colorWidth = (ushort)frameDescription.Width;
                this.colorHeight = (ushort)frameDescription.Height;
                this.bytesPerPixel = 4; //will use bgra color format

                this.bodies = new Body[this.BodyCount];

                // open the reader for the multiple sources frames
                this.multiReader = this.kinectSensor.OpenMultiSourceFrameReader(
                                                        FrameSourceTypes.Color | 
                                                        FrameSourceTypes.Depth | 
                                                        FrameSourceTypes.Body |
                                                        FrameSourceTypes.BodyIndex |
                                                        //FrameSourceTypes.Infrared |
                                                        0 );


                // allocate space to put the pixels being received and converted
                this.depthData = new ushort[this.depthWidth * this.depthHeight];
                this.infraRedData = new ushort[this.infraRedWidth * this.infraRedHeight];
                this.bodyData = new byte[this.bodyWidth * this.bodyHeight];

                this.depthCV = new Mat(this.depthHeight, this.depthWidth, MatType.CV_16UC1);
                this.irCV = new Mat(this.infraRedHeight, this.infraRedWidth, MatType.CV_16UC1);

                this.colorData = new byte[this.colorWidth * this.colorHeight * this.bytesPerPixel];
                this.smallColorHeight =(ushort) (this.colorHeight / 2);
                this.smallColorWidth = (ushort)(this.colorWidth / 2);
                this.colorSmallData = new byte[this.smallColorWidth * this.smallColorHeight * this.bytesPerPixel];
                this.colorCV = new Mat(this.colorHeight, this.colorWidth, MatType.CV_8UC4);

                if (this.multiReader != null)
                {
                    this.multiReader.MultiSourceFrameArrived += this.multiFrameArrived;
                }
                this.ticker.Enabled = false;
                this.ticker.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                this.ticker.AutoReset = true;
                this.ticker.Stop();

            }
            else
            {
                // on failure, set the status text
                System.Console.WriteLine("Could not initialize of find the Kinect.\n Is the kinect Service started ?");
            }
        }


        // play nice and release the resources used when exiting the program
        //
        public void dispose()
        {
            
            // stop possible recordings and flush the data to the HDD.
            if (null != this.recorder)
            {
                this.ticker.Stop();
                this.ticker.Enabled = false;
                this.recorder.Close();
                this.recorder.Dispose();
                if (null != this.skelWriter)
                {
                    this.skelWriter.Close();
                    this.skelWriter = null;
                }

                if (null != this.colorWriter)
                {
                    this.colorWriter.Dispose();
                    this.colorWriter = null;
                }
            }
            
            // stop the acquisition of frames from the kinect
            if (this.multiReader != null)
            {
                // BodyFrameReder is IDisposable
                this.multiReader.Dispose();
                this.multiReader = null;
            }

            // housecleaning in the audio dept
            if (this.audioReader != null)
            {
                // AudioBeamFrameReader is IDisposable
                this.audioReader.Dispose();
                this.audioReader = null;
            }

            /*
            // Body is IDisposable
            // clean up the crime scene 
            if (this.bodies != null)
            {
                foreach (Body body in this.bodies)
                {
                    if (body != null)
                    {
                        body.Dispose();
                    }
                }
            }
            */


            //release  the kinect.
            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

         

        }



        /// <summary>
        /// Handles the audio frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, AudioBeamFrameArrivedEventArgs e)
        {
            AudioBeamFrameReference frameReference = e.FrameReference;
           
            try
            {
                AudioBeamFrameList frameList = frameReference.AcquireBeamFrames();
              
                if (frameList != null)
                {
                    // AudioBeamFrameList is IDisposable
                    using (frameList)
                    {
                        // Only one audio beam is supported. Get the sub frame list for this beam
                        IReadOnlyList<AudioBeamSubFrame> subFrameList = frameList[0].SubFrames;
                        // Loop over all sub frames, extract audio buffer and beam information

                        double min = 0;
                        double max = 0;
                        foreach (AudioBeamSubFrame subFrame in subFrameList)
                        {
          
                            // Check if beam angle and/or confidence have changed
                            this.beamAngle = subFrame.BeamAngle;
                            this.beamAngleConfidence = subFrame.BeamAngleConfidence;
                            subFrame.CopyFrameDataToArray(this.audioBuffer);
                            // need to find the fft 
                            float intensity = 0;
                            for (int i = 0; i < this.audioBuffer.Length; i += BytesPerSample)
                            {
                                // Extract the 32-bit IEEE float sample from the byte array
                                float audioSample = BitConverter.ToSingle(this.audioBuffer, i);
                                if (audioSample < min) min = audioSample;
                                if (audioSample > max) max = audioSample;
                                this.audioSignal[i / BytesPerSample] = audioSample;
                                intensity += audioSample * audioSample;
                            }
                            intensity = (float)Math.Sqrt(intensity/this.audioSignal.Length);
                            if (intensity > 1E-3)
                            {
                               // Console.WriteLine( "{0},{1},{2}",intensity, min, max);
                            }
                           
                        }
                        /*
                         * 
                        WaveFormat waveFormat= new WaveFormat(16000,32,1);
                        WaveFileWriter waveFile= new WaveFileWriter("test.wav",waveFormat);
                        waveFile.Write(this.audioBuffer, 0, this.audioBuffer.Length);
                         * 
                         */
                    }
                }
            }
            catch (Exception)
            {
                // Ignore if the frame is no longer available
            }
        }

        
        /// <summary>
        /// Save the frame(s) at each (1000/fps) ms.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {

            // BUG: see https://bitbucket.org/opri/k2-series/issue/1

            ushort[] tempIR = new ushort[this.infraRedData.Length];
            this.infraRedData.CopyTo(tempIR,0);
            ushort[] tempDepth = new ushort[this.depthData.Length];
            this.depthData.CopyTo(tempDepth, 0);
            //qwConsole.WriteLine(DateTime.Now.ToString());
            
            lock(thisLock)
            {
                if (null != this.recorder)
                {
                    if (this.kinectSensor.InfraredFrameSource.IsActive)
                    {

                        //this.recorder.Write(this.infraRedData.ToString().ToCharArray());
                        foreach (ushort ir in tempIR)
                        {
                            this.recorder.Write(ir);
                        }
                    }
                    if (this.kinectSensor.DepthFrameSource.IsActive)
                    {
                        foreach (ushort dp in tempDepth)
                        {
                            this.recorder.Write(dp);
                        }
                    }
                    if (this.kinectSensor.BodyIndexFrameSource.IsActive)
                    {
                        this.recorder.Write(this.bodyData);
                    }
                   
                    if (this.kinectSensor.AudioSource.IsActive)
                    {
                        this.recorder.Write((float)this.beamAngle);
                        this.recorder.Write((float)this.beamAngleConfidence);
                    }

                    if (this.kinectSensor.BodyFrameSource.IsActive)
                    {


                        foreach (Body body in this.bodies)
                        {
                            if (null != body && body.IsTracked)
                            {
                                IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                                // convert the joint points to depth (display) space
                                Dictionary<JointType, Vec3f> jointPoints = new Dictionary<JointType, Vec3f>();
                                this.skelWriter.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}",
                                     body.TrackingId, body.IsTracked, body.HandRightState, body.HandRightConfidence, body.HandLeftState, body.HandLeftConfidence);
                                foreach (JointType jointType in joints.Keys)
                                {
                                    JointOrientation jo = body.JointOrientations[jointType];

                                    // mmmm need to figure out the coordinate mapper job and how to calculate real world measures
                                    DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(joints[jointType].Position);
                                    // see MS  K2API slideset slides 54-58
                                    this.skelWriter.Write("{0}, {1}, {2}, {3}, {4}", jointType, joints[jointType].TrackingState, depthSpacePoint.X, depthSpacePoint.Y, joints[jointType].Position.Z);
                                    this.skelWriter.WriteLine(", {0}, {1}, {2}, {3}", jo.Orientation.W, jo.Orientation.X, jo.Orientation.Y, jo.Orientation.Z);
                                }
                            }
                        }
                        if (null != this.skelWriter)
                        {
                            this.skelWriter.WriteLine(".");
                        }
                    }

                    if (this.kinectSensor.ColorFrameSource.IsActive && null != this.colorWriter)
                    {
                        /* removed in favor of writing the color channel in an AVI file
                        this.recorder.Write(this.colorData);
                         */
                        Mat converted = new Mat();
                        //Cv2.CvtColor(this.colorCV, converted, ColorConversion.BgraToBgr);
                        Cv2.Resize(this.colorCV, converted, new OpenCvSharp.CPlusPlus.Size(this.smallColorWidth, this.smallColorHeight));
                        this.colorWriter.Write(converted);
                    }
                }
                if (this.ticker.Enabled == false)
                {
                    if (null != this.recorder)
                    {
                        this.recorder.Close();
                        this.recorder.Dispose();
                        this.recorder = null;
                    }
                    if (null != this.skelWriter)
                    {
                        this.skelWriter.Close();
                        this.skelWriter = null;
                    }
                    if (null != this.colorWriter)
                    {
                        this.colorWriter.Dispose();
                        this.colorWriter = null;
                    }
                }
            } 

        }


        /// <summary>
        /// Draws the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void multiFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            // Copy the pixel data from the image to a temporary array
            MultiSourceFrameReference frameReference = e.FrameReference;
            // Acquire the color frame
            MultiSourceFrame frame = frameReference.AcquireFrame();

            using (DepthFrame drf = frame.DepthFrameReference.AcquireFrame())
            {
                if (null != drf)
                {
                    lock (thisLock)
                    {
                        drf.CopyFrameDataToArray(this.depthData);
                    }
                   
                }
            }

            using (BodyIndexFrame bif = frame.BodyIndexFrameReference.AcquireFrame()) 
            {
                if (null != bif)
                {
                    lock (thisLock)
                    {
                        // Copy the pixel data from the image to a temporary array
                        bif.CopyFrameDataToArray(this.bodyData);
                    
                    }
                    
                }
            }

            using (InfraredFrame irf = frame.InfraredFrameReference.AcquireFrame())
            {
                if (null != irf)
                {
                    lock (thisLock)
                    {
                        // Copy the pixel data from the image to a temporary array
                        irf.CopyFrameDataToArray(this.infraRedData);
                    }
                }
            }
            using (BodyFrame bf = frame.BodyFrameReference.AcquireFrame())
            {
                if (null!= bf)
                {
                    bf.GetAndRefreshBodyData(this.bodies);
                        
                }
            }
            using (ColorFrame cf = frame.ColorFrameReference.AcquireFrame())
            {
                if (null != cf)
                {
                    lock (thisLock)
                    {
                        // Copy the pixel data from the image to a temporary array
                        cf.CopyConvertedFrameDataToArray(this.colorData,ColorImageFormat.Bgra);
                        this.colorCV = new Mat(this.colorHeight, this.colorWidth, MatType.CV_8UC4, this.colorData);
                    }
                }
            }
        }


        public void movieStartStop(string filename)
        {
            this.ticker.Enabled = !this.ticker.Enabled;
            if (this.ticker.Enabled)
            {
                if (this.recorder != null)
                {
                    this.recorder.Dispose();
                }

                this.recorder = new BinaryWriter(File.Open(filename, FileMode.Create));
                byte nChannels = 0;
                if (this.kinectSensor.InfraredFrameSource.IsActive) nChannels++;
                if (this.kinectSensor.DepthFrameSource.IsActive) nChannels++;
                // extracting the  color channel from the opri file in favor of an avi file
                //if (this.kinectSensor.ColorFrameSource.IsActive) nChannels++;
                if (this.kinectSensor.BodyIndexFrameSource.IsActive) nChannels++;
                if (this.kinectSensor.AudioSource.IsActive) nChannels++;
                Console.WriteLine("Number of Channels recorded : {0}", nChannels);

                this.recorder.Write("OPRI".ToCharArray());

                this.recorder.Write((byte)this.fps);
                this.recorder.Write(nChannels);
                

                if (this.kinectSensor.InfraredFrameSource.IsActive) { 
                    this.recorder.Write("IF".ToCharArray());
                    this.recorder.Write(this.infraRedWidth); this.recorder.Write(this.infraRedHeight); this.recorder.Write((byte)16);
                }
                if (this.kinectSensor.DepthFrameSource.IsActive)
                {
                    this.recorder.Write("DP".ToCharArray());
                    this.recorder.Write(this.depthWidth); this.recorder.Write(this.depthHeight); this.recorder.Write((byte)12);
                }
                if (this.kinectSensor.BodyIndexFrameSource.IsActive)
                {
                    this.recorder.Write("BD".ToCharArray());
                    this.recorder.Write(this.bodyWidth); this.recorder.Write(this.bodyHeight); this.recorder.Write((byte)3);
                }
                if (this.kinectSensor.ColorFrameSource.IsActive)
                {
                    /* 
                     * removed in favor of separate avi file 
                    this.recorder.Write("CO".ToCharArray());
                    this.recorder.Write(this.colorWidth); this.recorder.Write(this.colorHeight); this.recorder.Write((byte)32);
                     */
                    this.colorWriter = new VideoWriter(filename.Replace(".opri", ".avi"), -1, this.fps, new OpenCvSharp.CPlusPlus.Size(this.smallColorWidth, this.smallColorHeight));
                }
                if (this.kinectSensor.AudioSource.IsActive)
                {
                    this.recorder.Write("BM".ToCharArray());
                    this.recorder.Write((ushort)2); this.recorder.Write((ushort)1); this.recorder.Write((byte)32);
                }

                if (this.kinectSensor.BodyFrameSource.IsActive)
                {

                    this.skelWriter = new StreamWriter(filename + ".skel");
                    // very crude mapper 
                    DepthSpacePoint dSP = new DepthSpacePoint();
                    ColorSpacePoint cSP;
                    dSP.X = this.depthWidth / 4;
                    dSP.Y = this.depthHeight / 4;
                    cSP = this.coordinateMapper.MapDepthPointToColorSpace(dSP, 2048);
                    //this.skelWriter.WriteLine("#####################");
                    //this.skelWriter.WriteLine("#  {0}, {1}, {2}, {3}", dSP.X, dSP.Y, cSP.X, cSP.Y);
                    Console.WriteLine("#  {0}, {1}, {2}, {3}", dSP.X, dSP.Y, cSP.X, cSP.Y);
                    dSP.X *= 3;
                    dSP.Y *= 3;

                    cSP = this.coordinateMapper.MapDepthPointToColorSpace(dSP, 2048);
                    //this.skelWriter.WriteLine("# {0}, {1}, {2}, {3}", dSP.X, dSP.Y, cSP.X, cSP.Y);
                    //this.skelWriter.WriteLine("#####################");
                    Console.WriteLine("# {0}, {1}, {2}, {3}", dSP.X, dSP.Y, cSP.X, cSP.Y);
                    this.skelWriter.WriteLine("Kinect2.0beta");
                    this.skelWriter.WriteLine("fps, {0}, bodies, {1}, joints, {2}", this.fps, 6, 25);
                }   
            }
            // else the writer will be closed and disposed in the next timer event
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="body">body to draw</param>
        /// <param name="color">color to draw</param>
        /// <param name="frame">opencv Mat to draw to</param>
        public void drawBody(Mat frame, Scalar color, Body body)
        {
            //boneStructure is purely static and should be a static member initialized at construct time
            JointType[,] boneStructure = new JointType[,] {
                         // Torso
                        {JointType.Head, JointType.Neck},
                        {JointType.Neck, JointType.SpineShoulder},
                        {JointType.SpineShoulder, JointType.ShoulderRight},
                        {JointType.SpineShoulder, JointType.ShoulderLeft},
                        
                        //Variant #1 from the sdk : spine and hip bone
                        //{JointType.SpineShoulder, JointType.SpineMid},
                        //{JointType.SpineMid, JointType.SpineBase},
                        //{JointType.SpineBase, JointType.HipRight},
                        //{JointType.SpineBase, JointType.HipLeft},
                        
                        //variant #2 hourglass shape
                        {JointType.ShoulderLeft, JointType.HipRight},
                        {JointType.ShoulderRight, JointType.HipLeft},
                        {JointType.HipRight, JointType.HipLeft},
                       
                        // Right Arm    
                        {JointType.ShoulderRight, JointType.ElbowRight},
                        {JointType.ElbowRight, JointType.WristRight},
                        {JointType.WristRight, JointType.HandRight},
                        {JointType.HandRight, JointType.HandTipRight},
                        {JointType.WristRight, JointType.ThumbRight},
                
                        // Left Arm
                        {JointType.ShoulderLeft, JointType.ElbowLeft},
                        {JointType.ElbowLeft, JointType.WristLeft},
                        {JointType.WristLeft, JointType.HandLeft},
                        {JointType.HandLeft, JointType.HandTipLeft},
                        {JointType.WristLeft, JointType.ThumbLeft}, 
                
                        //Right Leg
                        {JointType.HipRight, JointType.KneeRight},
                        {JointType.KneeRight, JointType.AnkleRight},
                        {JointType.AnkleRight, JointType.FootRight},

                        // Left Leg
                        {JointType.HipLeft, JointType.KneeLeft},
                        {JointType.KneeLeft, JointType.AnkleLeft},
                        {JointType.AnkleLeft, JointType.FootLeft}
                };
            
            
            if (null != body && body.IsTracked)
            {
                IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                // convert the joint points to depth (display) space
                Dictionary<JointType, DepthSpacePoint> depthSpacePoint = new Dictionary<JointType, DepthSpacePoint>();
                foreach (JointType jointType in joints.Keys)
                {
                    depthSpacePoint[jointType] = this.coordinateMapper.MapCameraPointToDepthSpace(joints[jointType].Position);
                }

                int nBone = boneStructure.Length / 2; // Length get the total number of elements in all the dimension hence divide by 2 in this case...

                for (int i = 0; i < nBone; i++)
                {
                    JointType jt0 = boneStructure[i, 0];
                    JointType jt1 = boneStructure[i, 1];
                    if (joints[jt0].TrackingState==TrackingState.Tracked && joints[jt1].TrackingState==TrackingState.Tracked ) 
                    {
                        frame.Line( (int)Math.Round(depthSpacePoint[jt0].X),(int)Math.Round(depthSpacePoint[jt0].Y),
                                    (int)Math.Round(depthSpacePoint[jt1].X),(int)Math.Round(depthSpacePoint[jt1].Y),
                                     color, 3);
                    }
                }


                // Draw the joints
                /*
                foreach (JointType jointType in joints.Keys)
                {
                    Brush drawBrush = null;

                    TrackingState trackingState = joints[jointType].TrackingState;

                    if (trackingState == TrackingState.Tracked)
                    {
                        drawBrush = this.trackedJointBrush;
                    }
                    else if (trackingState == TrackingState.Inferred)
                    {
                        drawBrush = this.inferredJointBrush;
                    }

                    if (drawBrush != null)
                    {
                        frame.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                    }
                }
                */
            }
        }
     
     }


    class Program
    {

        //Main program loop
        public static int Main(string[] args)
        {
            myK2 sensor = new myK2();

            int fps =30; 
            
            sensor.init(fps);
            OpenCvSharp.CPlusPlus.Size dsize = new OpenCvSharp.CPlusPlus.Size(sensor.depthWidth, sensor.depthHeight);
            Mat anonymous = new Mat(sensor.depthHeight, sensor.depthWidth, MatType.CV_8UC3);
            Mat irCV = new Mat(sensor.depthHeight, sensor.depthWidth, MatType.CV_16U);
            ushort[] dataBuffer = new ushort[sensor.depthData.Length];
            int numFrame = 0;
            var indexerAnonymous= anonymous.GetGenericIndexer<Vec3b>();
            Vec3b pixel= new Vec3b();
            int key = 0;
            Console.WriteLine("Channel Color: {0}", sensor.kinectSensor.ColorFrameSource.IsActive);
            Console.WriteLine("Channel Depth: {0}", sensor.kinectSensor.DepthFrameSource.IsActive);
            Console.WriteLine("Channel InfraRed: {0}", sensor.kinectSensor.InfraredFrameSource.IsActive);
            Console.WriteLine("Channel Body: {0}", sensor.kinectSensor.BodyFrameSource.IsActive);
            Console.WriteLine("Channel BodyIndex: {0}", sensor.kinectSensor.BodyIndexFrameSource.IsActive);
            Console.WriteLine("Channel Audio: {0}", sensor.kinectSensor.AudioSource.IsActive);
            Cv2.NamedWindow("Combined View", WindowMode.ExpandedGui);

            OpenCvSharp.CPlusPlus.Point[] shape = new OpenCvSharp.CPlusPlus.Point[3];
            shape[0]=new OpenCvSharp.CPlusPlus.Point(-20, 40);
            shape[1]=new OpenCvSharp.CPlusPlus.Point( 20, 40);
            shape[2]=new OpenCvSharp.CPlusPlus.Point(  0, 90);

            //sensor.movieStartStop(string.Format(@"{1}\K2\kinect2-movie{0}.opri", DateTime.Now.ToString("hh.mm.ss"), 
            //                                    Environment.GetFolderPath(Environment.SpecialFolder.Personal)));


            Mat smallColor = new Mat(sensor.smallColorHeight, sensor.smallColorWidth, MatType.CV_8UC3);

            while (key != 27)   
            {
                int x = 0; int y = 0;
                int i = 0;

                foreach (ushort u in sensor.depthData)
                {
                    pixel.Item1 = (byte)((u % 256));
                    pixel.Item2 = (byte)((u / 256) * 8);
                    pixel.Item0 = sensor.bodyData[i];
                    indexerAnonymous[y, x] = pixel;

                    x++;
                    i++;

                    if (x >= 512)
                    {
                        x = 0;
                        y++;
                    }
                }


                foreach (Body body in sensor.bodies)
                {
                    if (null != body && body.IsTracked)
                    {
                        IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                        // convert the joint points to depth (display) space
                        Dictionary<JointType, Vec3f> jointPoints = new Dictionary<JointType, Vec3f>();
                        foreach (JointType jointType in joints.Keys)
                        {

                            //Console.WriteLine("{0}, {1}", jointType, joints[jointType].JointType);
                            JointOrientation jo = body.JointOrientations[jointType];
                            DepthSpacePoint depthSpacePoint = sensor.coordinateMapper.MapCameraPointToDepthSpace(joints[jointType].Position);
                            jointPoints[jointType] = new Vec3f(depthSpacePoint.X, depthSpacePoint.Y, 0f);
                        }

                        sensor.drawBody(anonymous, Scalar.LightCyan, body);
                    }

                }
                int xBeam = (int)Math.Round(256 * (1 + Math.Sin(sensor.beamAngle * 2)));

                if (sensor.beamAngleConfidence > 0.02)
                {
                    //anonymous.Line(xBeam, 0, xBeam, 50, Scalar.Yellow, (int)(1 + 5 * sensor.beamAngleConfidence));
                    shape[0].X = xBeam - 20;
                    shape[1].X = xBeam + 20;
                    shape[2].X = xBeam;

                    OpenCvSharp.CPlusPlus.Point[][] oneShape = new OpenCvSharp.CPlusPlus.Point[][] { shape };
                    anonymous.FillPoly(oneShape, Scalar.Red);
                    anonymous.Polylines(oneShape, true, Scalar.Silver, 2);
                }
                if (sensor.ticker.Enabled)
                {
                    anonymous.Circle(15, 15, 10, Scalar.Red, -1);
                }
                irCV.SetArray(0, 0, sensor.infraRedData);
                
                Cv2.Flip(anonymous, anonymous, FlipMode.Y);
                Cv2.ImShow("Combined View", anonymous);
                Cv2.ImShow("infrared", irCV*8);
                Cv2.ImShow("Color", sensor.colorCV.Resize(new  OpenCvSharp.CPlusPlus.Size(sensor.smallColorWidth, sensor.smallColorHeight)));

                key = Cv2.WaitKey(65);


                switch (key)
                {
                    case 27:
                        Console.WriteLine("Escape, bye");
                        break;
                    case 66: // 'B'
                    case 98: // 'b'


                        break;
                    case 70: //'F
                    case 102://'f'
                        sensor.movieStartStop(string.Format(@"{1}\K2\kinect2-movie{0}.opri", DateTime.Now.ToString("hh.mm.ss"),
                                         Environment.GetFolderPath(Environment.SpecialFolder.Personal)));

                        break;

                    case 73:
                    case 105: //'I' or 'i'
                        Cv2.ImWrite(string.Format(@"{1}\K2\infra-{0}.png", DateTime.Now.ToString("hh.mm.ss"),
                                    Environment.GetFolderPath(Environment.SpecialFolder.Personal)),
                        new Mat(sensor.infraRedHeight, sensor.infraRedWidth, MatType.CV_16U, sensor.infraRedData));
                        Console.WriteLine("InfraRed Snapshot");
                        break;
                    case 68:
                    case 100: //'D' or 'd'
                        Cv2.ImWrite(string.Format(@"{1}\K2\depth-{0}.png", DateTime.Now.ToString("hh.mm.ss"),
                                    Environment.GetFolderPath(Environment.SpecialFolder.Personal)),
                        new Mat(sensor.depthHeight, sensor.depthWidth, MatType.CV_16U,sensor.depthData));
                        Console.WriteLine("Depth map snapshot");
                        break;
                    default:
                        //Console.WriteLine("Key was :{0}:",key);
                        break;

                }
                numFrame++;

            }   

            sensor.dispose();
            return 0;

        }
        
    }
}
